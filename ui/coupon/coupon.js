// ui/coupon/coupon.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    couponPrice: {
      type: String,
      value: ''
    },
    couponType: {
      type: String,
      value: ''
    },
    couponDate: {
      type: String,
      value: ''
    },
    couponDesc: {
      type: String,
      value: ''
    },
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
