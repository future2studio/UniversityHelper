// ui/manageItem/manageItem.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    name:{
      type:String,
      value:""
    },
    sex: {
      type: String,
      value: ""
    },
    idCard: {
      type: String,
      value: ""
    },
    running: {
      type: String,
      value: ""
    },
    complete: {
      type: String,
      value: ""
    },
    complaint: {
      type: String,
      value: ""
    },
    money: {
      type: String,
      value: ""
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    toNextPage:function(){
      wx.navigateTo({
        url: '/pages/userCenter/teamManage/memberManage/index',
      })
    }
  }
})
