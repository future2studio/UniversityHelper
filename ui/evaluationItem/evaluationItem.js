// ui/evaluationItem/evaluationItem.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    userName:{
      type:String
    },
    avatar:{
      type:String
    },
    time:{
      type:String
    },
    content:{
      type:String
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
