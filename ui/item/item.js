// ui/item.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    leftLable:{
      type:String,
      value:"" 
    },
    rightLable: {
      type: String,
      value: ""
    },
    textBody: { 
      type: String,
      value: ""
    },
    money: {
      type: String,
      value: ""
    },
    address: {
      type: String,
      value: ""
    },
    goodArray: {
      type: Array,
    },
    itemType: {
      type: String,
      value: ""
    },
    orderType:{
      type:String,
      value:""
    },
    userState:{
      type:String,
      value:""
    },
    runnerState:{
      type:String,
      value:""
    },
    index:{
      type:Number
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    //追加赏金按钮
    toAddMoney:function(e){
      let id = e.currentTarget.dataset.id
      wx.navigateTo({
        url: '/pages/publishMission/index?id='+id,
      })
    },
    //评价页面
    toEvaluation:function(){
      wx.navigateTo({
        url: '/pages/userCenter/publishOrder/evaluation/index',
      })
    },
    //订单详情
    toOrderDetail:function(){
      wx.navigateTo({
        url: '/pages/missionDetail/index',
      })
    }
  }
})
