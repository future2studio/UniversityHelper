// ui/goodItem/goodItem.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    goodName:{
      type:String,
    },
    goodPrice: {
      type: Number,
    },
    goodNum: {
      type: Number,
    },
    index: {
      type: String,
    },
    singlePrice:{
      type: Number
    },
    goodSrc:{
      type:String
    }
    
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    // bindChange(){
    //   this.triggerEvent('bindChange',{
    //     goodNum:this.properties.goodNum,
    //     index:this.properties.index,
    //     goodPrice:this.properties.goodPrice
    //   })
    // },
    onChange:function(e){
      console.log(e);
      let price = this.properties.goodPrice;
      let newPrice = this.properties.singlePrice * e.detail;
      this.setData({
        goodNum:e.detail,
        goodPrice:newPrice
      })
      // this.bindChange();
      let index = e.currentTarget.dataset.index ;
      let array = wx.getStorageSync('shopCar');
      for(var i = 0 ;i<array.length;i++){
        if(i == index){
          array[i].price = this.properties.goodPrice;
          array[i].num = this.properties.goodNum;
        }
      }
      wx.setStorageSync("shopCar", array);
      // this.totalPrice(array);
      var sum = 0;
      for (var i = 0; i < array.length; i++) {
        console.log("goodPrice:" + array[i].price)
        sum = array[i].price + sum;
     
      }
      console.log("sum:" + sum)
      this.triggerEvent("myevent", sum);
    },
    onClose(event) {
      var _this = this ;
      const { position, instance } = event.detail;
      switch (position) {
        case 'right':
            console.log("right")
            console.log(event);
            instance.close();
          // _this.deleteShopCar(event.currentTarget.dataset.index);
          this.triggerEvent("refresh", event.currentTarget.dataset.index)
          break;
      }
    },

  }
})
