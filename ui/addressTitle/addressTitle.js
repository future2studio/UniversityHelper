// ui/addressTitle/addressTitle.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item:{
      type:Object,
    },
    hide:{
      type:Boolean,
      value:false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    toNextPage:function(e){
      var id = e.currentTarget.dataset.id ;
      wx.navigateTo({
        url: '/pages/userCenter/userInfo/address/index?id='+id,
      })
    }
  }
})
