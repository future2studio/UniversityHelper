// ui/addressItem/addressItem.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    index:{
      type:String,

    },
    userName: {
      type: String,

    },
    phone: {
      type: String,
  
    },
    address: {
      type: String,

    },
    item:{
      type:Object
    },
    current:{
      type:Number
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    backNavigation:function(e){
      var pages = getCurrentPages();
      var prevPage = pages[pages.length - 2] ;
      var item = e.currentTarget.dataset.item ;
      console.log(e);
      prevPage.setData({
        item:item,
        addressHide:true
      })
      wx.navigateBack({
        delta:1
      })
    }
  }
})
