const App = getApp();
var HOST_URL ='https://www.guizhousuxing.top:8091/api';
var requestHandler = {
  success:function(res){},
  fail:function(res){},
}

//GET请求
function requestGet(url, data, requestHandler){
  requestget(url, data,requestHandler );
}
function requestget(url, data, requestHandler)
{
  wx.request({
    url: HOST_URL+url,
    data: data,
    method: 'GET',
    header: { 'content-type': 'application/json'},
    success:function(res){
      requestHandler.success(res.data);
    },
    fail:function(res){
      requestHandler.fail(res.data);
    },
    complete:function(){},
  })
}
//POST请求
function requestPost(url, data,requestHandler){
  requestpost(url,  data,requestHandler);
}
function requestpost(url,data,requestHandler) {
  
  wx.request({
    url: HOST_URL + url,
    data: data,
    method: 'POST',
    header: { 'content-type': 'application/json' },
    success: function (res) {
      console.log(res)
      if(res.data.code == 500)
      {
        return false;
      }
      else{
        requestHandler.success(res.data);
    }
    },
    fail: function () {
      requestHandler.fail();
    },
    complete: function () {},
  })
}
module.exports = {
  requestGet: requestGet,
  requestPost: requestPost
}

