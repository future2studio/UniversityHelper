// pages/table/index.js
const app = getApp()
Page({
  data: {
    school:"贵州财大",
    location:"",
    TabCur: 1,
    scrollLeft: 0,
    good:[{
      'goodsNum':2,
      'goodPrice':30,
      'goodTitle':'康师傅红烧牛肉面',
      'goodAvatar':'https://ossweb-img.qq.com/images/lol/img/champion/Morgana.png'
    },
    {
      'goodsNum': 2,
      'goodPrice': 30,
      'goodTitle': '康师傅红烧牛肉面',
      'goodAvatar': 'https://ossweb-img.qq.com/images/lol/img/champion/Morgana.png'
    }],
    list: [{ 'time': '2019-02-02', 'price': '2', 'address': '财大丹桂苑', 'content': '帮我带两包烟，玉溪和3个5', 'itemType': 1 },
      { 'time': '2019-02-02', 'price': '2', 'address': '财大丹桂苑', 'content': '帮我带两包烟，玉溪和3个5', 'itemType': 0 }, { 'time': '2019-02-02', 'price': '2', 'address': '财大丹桂苑', 'content': '帮我带两包烟，玉溪和3个5', 'itemType': 0 }, { 'time': '2019-02-02', 'price': '2', 'address': '财大丹桂苑', 'content': '帮我带两包烟，玉溪和3个5', 'itemType': 1 }],
    show:false,
    openState:false,
    schoolList: [{ 'school': '贵州财大', 'list': [{ 'index': 0, 'name': '丹桂苑1' }, { 'index': 1, 'name': '丹桂苑左边1' },
      { 'index': 2, 'name': '丹桂苑右边1' }]
    }, {
      'school': '贵州大学', 'list': [{ 'index': 0, 'name': '丹桂苑2' }, { 'index': 1, 'name': '丹桂苑左边2' },
        { 'index': 2, 'name': '丹桂苑右边2' }]
      },
      {
        'school': '贵州师范', 'list': [{ 'index': 0, 'name': '丹桂苑3' }, { 'index': 1, 'name': '丹桂苑左边3' },
          { 'index': 2, 'name': '丹桂苑右边3' }]
      },
      {
        'school': '贵州医科大', 'list': [{ 'index': 0, 'name': '丹桂苑4' }, { 'index': 1, 'name': '丹桂苑左边4' },
          { 'index': 2, 'name': '丹桂苑右边4' }]
      },
      {
        'school': '贵州家里蹲', 'list': [{ 'index': 0, 'name': '丹桂苑5' }, { 'index': 1, 'name': '丹桂苑左边5' },
          { 'index': 2, 'name': '丹桂苑右边5' }]
      },]
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  tabSelect(e) {
    console.log(e);
    this.setData({
      TabCur: e.currentTarget.dataset.id,
      scrollLeft: (e.currentTarget.dataset.id - 1) * 60
    })
  },
  toPublishMission:function(){
    let state = this.data.openState ;
    if(!state){
      this.setData({
        show: true,
        openState:true,
        name:"fade-right"
      })
    }else{
      this.setData({
        show: false,
        openState: false
      })
    }
   
    // wx.navigateTo({
    //   url: '/pages/publishMission/index',
    // })
  },
  toMission:function(){
     wx.navigateTo({
      url: '/pages/publishMission/index',
    })
  },
  onShow:function(){
    var school = this.data.school ;
    wx.setNavigationBarTitle({
      title: school,
    })
  },
  onHide:function(){
    this.setData({
      show:false,
      openState: false
    })
  },
  getSchoolItem:function(e){
    var item = e.currentTarget.dataset.item ;
    console.log(e);
    var list = item.list ;
    var school = item.school;
    this.setData({
      chlidList:list,
      school: school
    })
  
    wx.setNavigationBarTitle({
      title: school,
    })
  },
  getLocation:function(e){
    var location = e.currentTarget.dataset.location ;
    this.setData({
      location:location
    })
    var school = this.data.school ;
    wx.setNavigationBarTitle({
      title: school+" - "+location,
    })
    this.hideModal();
  }
})