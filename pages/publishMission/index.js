// pages/publishMission/index.js
Page({
 
  /**
   * 页面的初始数据
   */
  data: {
    columns: ['代跑', '代取'], 
    index: 0,
    pages: ['/pages/userCenter/userInfo/address/index','/pages/userCenter/counpon/index'] ,
    totalPrice:200
  },
  onChange:function(e){
    var e = e.detail * 100 ;
    this.setData({
      totalPrice:e
    })
  },
  /**
   * 生命周期函数--监听页面加载 
   */
  onLoad: function (options) {
    //获取本地默认收货地址
    var item = wx.getStorageSync('address');
    if (item) {
      this.setData({
        addressHide: true,
        item: item
      })
      console.log(item)
    } else {
      this.setData({
        addressHide: false
      })
      console.log("else")
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
 
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  /**
   * 需求类型切换
   */
  bindPickerChange:function(e){
    let index = e.detail.value;
    this.setData({
      index:index
    })
  },
  /**
   * 跳转收货地址或优惠券路由
   */
  toNextPage:function(e){
    console.log(e);
    let pages = this.data.pages ;
    let id = e.currentTarget.dataset.id ;
    wx.navigateTo({
      url: pages[id],
    })
  }
})