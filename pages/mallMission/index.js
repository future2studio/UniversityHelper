// pages/mallMission/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ptmoney:200
  },
  /**
   * 步进器修改商品数量
   */
  onChange(e){
    // console.log("bindChange")
    console.log("e:"+e.detail);
    var e = e.detail * 100 ;
    var ptmoney = this.data.ptmoney ;
    this.setData({
      goodTotalPrice:e,
      totalPrice:e+ptmoney
    })
  },
  /**
   * 删除商品后刷新数据
   */
  refreshList:function(e){
    console.log("refreshList")
    var index = e.detail ;
    this.deleteShopCar(index) ;
  },
  /**
   * 删除商品后刷新数据
   */
  deleteShopCar: function (index) {
    let array = wx.getStorageSync('shopCar');
    console.log("len:" + array.length);
    for (var i = 0; i < array.length; i++) {
      if (i == index) {
        array.splice(index, 1);
      }
    }
    console.log("len:" + array.length);
    this.setData({
      array: array
    })
    if(array.length == 0){
      wx.clearStorageSync();
      this.setData({
        view:false
      })
    }else{
      wx.setStorageSync('shopCar', array);
      this.calculation();
    }
    
  },
  /**
   * 计算购物车商品和跑腿费总价格
   */
  calculation:function(){
    //获取购物车缓存数据
    let array = wx.getStorageSync('shopCar');
    if (array) {
      this.setData({
        view: true,
      })
      let total = 0;
      for (var i = 0; i < array.length; i++) {
        let price = array[i].price;
        total = total + price;
        console.log("total=" + total)
      }
      let totalprice = total * 100;
      let ptmoney = this.data.ptmoney;
      let tp = ptmoney + totalprice;
      this.setData({
        goodTotalPrice: totalprice,
        array: array,
    
        totalPrice: tp
      })
    } else {
      view: false
    }
  },
  /**
   * 跑腿费用步进器计算
   */
  onptChange(e){
    console.log(e);
    var ptmoney = this.data.ptmoney ;
    var e = e.detail * 100;
    var total = 0 ;
    var gt = this.data.goodTotalPrice;
    var tp = this.data.totalPrice ;
    if(e > ptmoney){
      total = gt + e;
    }else{
      var c = ptmoney - e;
      total = tp - c;
    }
    this.setData({
      ptmoney: e,
      totalPrice:total,
    })
  },
  /**
   * 提交表单
   */
  onSubmit(){ 
    console.log("onSubmit")
    let total = 0;
    let array = this.data.array;
    for (var i = 0; i < array.length; i++) {
      let price = array[i].price;
      total = total + price;
      
    }
    console.log("total=" + total)
  },
  toNextPage:function(e){
    var id = e.currentTarget.dataset.id ;
    wx.navigateTo({
      url: '/pages/userCenter/counpon/index?id='+id,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.calculation();
    //获取本地默认收货地址
    var item = wx.getStorageSync('address');
    if (item) {
      this.setData({
        addressHide: true,
        item: item
      })
    console.log(item)
    } else {
      this.setData({
        addressHide: false
      })
      console.log("else")
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})