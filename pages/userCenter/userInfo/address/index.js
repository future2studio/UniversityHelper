// pages/userCenter/userInfo/address/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    addressArray:[{'name':'张三','phone':'17777777777','address':'贵州财大丹桂苑','xaddress':'7栋505','current':1},
      { 'name': '李四', 'phone': '17777777777', 'address': '贵州财大丹桂苑', 'xaddress': '6栋505', 'current': 0 },
      { 'name': '王二', 'phone': '17777777777', 'address': '贵州财大丹桂苑', 'xaddress': '4栋505', 'current': 0 },
      { 'name': '麻子', 'phone': '17777777777', 'address': '贵州财大丹桂苑', 'xaddress': '5栋505', 'current': 0 },
      { 'name': '瓜皮', 'phone': '17777777777', 'address': '贵州财大丹桂苑', 'xaddress': '3栋505', 'current': 0 },]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  toNextPage:function(){
    wx.navigateTo({
      url: '/pages/userCenter/userInfo/newAddress/index',
    })
  }
})